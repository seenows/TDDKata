using System;
using Core;
using NUnit.Framework;
using FluentAssertions;

namespace Tests
{
    public class StringCalculatorTests
    {
        [Test]
        public void WhenArgsIsEmptyString_ThenShouldReturnZero()
        {
            // arrange
            var calc = new StringCalculator();

            // act
            var result = calc.Sum(string.Empty);

            // assert
            result.Should().Be(0);
        }

        [TestCase("42")]
        public void WhenArgsIsSingleNumber_ThenReturnThisNumber(string number)
        {
            // arrange
            var calc = new StringCalculator();

            // act
            var result = calc.Sum(number);

            // assert
            result.Should().Be(int.Parse(number));
        }

        [TestCase("1,2", 3)]
        [TestCase("1,2,3,4", 10)]
        public void WhenArgsAreTwoNumbersWithComma_ThenShouldReturnItSum(string args, int expectedValue)
        {
            // arrange
            var calc = new StringCalculator();

            // act
            var result = calc.Sum(args);

            // assert
            result.Should().Be(expectedValue);
        }

        [TestCase("1\n2", 3)]
        public void WhenUsedLineBreaksInArgs_ThenShouldReturnCorrectSum(string args, int expectedValue)
        {
            // arrange
            var calc = new StringCalculator();

            // act
            var result = calc.Sum(args);

            // assert
            result.Should().Be(expectedValue);
        }

        [TestCase("//;\n1;2;3", 6)]
        public void WhenArgsCanContainSettingsForSeparator_ThenSumShouldWorkWithIt(string args, int expectedValue)
        {
            // arrange
            var calc = new StringCalculator();

            // act
            var result = calc.Sum(args);

            // assert
            result.Should().Be(expectedValue);
        }

        [TestCase("1,-1,2")]
        public void WhenArgsContainsNegativeNumbers_ThenThrowException(string args)
        {
            // arrange
            var calc = new StringCalculator();
            var exceptionHandled = false;

            // act
            try
            {
                calc.Sum(args);
            }
            catch (Exception e)
            {
                exceptionHandled = true;
            }
            
            // assert
            exceptionHandled.Should().BeTrue();
        }
    }
}